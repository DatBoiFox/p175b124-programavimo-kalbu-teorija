print ("Hello world")

#           Days
# SUN MON TUE WED THU FRI SAT

#                      Months
# JAN FEB   MAR APR MAY JUN JUL AUG SEP OCT NOV DEC
# 31  28/29 31  30  31  30  31  31  30  31  30  31

class Week:
    def __init__(self, day, dayNumber):
        self.day = day
        self.dayNumber = dayNumber

class WeekCycle(Week):
    def __init__(self, day, dayNumber, currentDay):
        super().__init__(day, dayNumber)
        self.currentDay = currentDay

    def nextDay(self):
        self.currentDay = self.currentDay + 1
        if(self.currentDay > 7):
            self.currentDay = 1
        self.day = getDayName(self.currentDay)
        self.dayNumber = self.currentDay
        
class Day:
    def __init__(self, day, number):
        
        self.day = day
        self.number = number

class MonthDay(Day):
    def __init__(self, month, day, number):
        super().__init__(day, number)
        self.month = month

def findDays():
    for i in months:
        if(i.month == im):
            startMonth.number = i.number
            break

def getDayNumber(givenDay):
    for i in weekDays:
        if(i.day == givenDay):
            return i.dayNumber
def getDayName(dayNumber):
    for i in weekDays:
        if(i.dayNumber == dayNumber):
            return i.day

def countWeekends():
    count = 0
    for i in range(startMonth.number):
        if(weekCycle.currentDay == 5 or weekCycle.currentDay == 6):
            count = count + 1
        weekCycle.nextDay()
    return count

def cleanArray(array):
    for i in range (N):
        array[i][1] = str(array[i][1]).rstrip()
weekDays = [
    Week("SUN", 7), 
    Week("MON", 1), 
    Week("TUE", 2), 
    Week("WED", 3), 
    Week("THU", 4), 
    Week("FRI", 5), 
    Week("SAT", 6)]

months = [
    MonthDay("JAN", "", 31),
    MonthDay("FEB", "", 28),
    MonthDay("MAR", "", 31),
    MonthDay("APR", "", 30),
    MonthDay("MAY", "", 31),
    MonthDay("JUN", "", 30),
    MonthDay("JUL", "", 31),
    MonthDay("AUG", "", 31),
    MonthDay("SEP", "", 30),
    MonthDay("OCT", "", 31),
    MonthDay("NOV", "", 30),
    MonthDay("DEC", "", 31)]


in_file = open("Input.txt", "r")
N = 0
Days = []
for line in in_file.readlines():
    if(N <= 0):
        N = int(line)
    else:
        Days.append(line.split(" "))

in_file.close()
cleanArray(Days)

out_file = open("Output.txt", "w")

for a in range(N):

    im = Days[a][0]
    id = Days[a][1]

    weekCycle = WeekCycle(id, getDayNumber(id), getDayNumber(id))


    startMonth = MonthDay(im, id, 0)
    findDays()

    #print(countWeekends())
    out_file.write(str(countWeekends()) + "\n")

out_file.close()

